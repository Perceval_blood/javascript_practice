// // for loop
// for (let index = 0; index < 5; index++) {
//   console.log(index);
// }

// const myArray = ["first", "second", "third"];

// for (let i = 0; i < myArray.length; i++) {
//   console.log(myArray[i]);
// }
// // forEach

// myArray.forEach((element, index) => {
//   console.log(element, index);
// });

// // while
// let i = 0;
// while (i < 5) {
//   console.log(i);
//   i++;
// }

// //do while
// let j = 0;
// do {
//   console.log(j);
//   j++;
// } while (j < 5);

const myObject = {
  x: 10,
  y: true,
  z: "abc",
};

for (const key in myObject) {
  console.log(key, myObject[key]);
}

Object.values(myObject).forEach((value) => {
  console.log(value);
});

const x = Object.values(myObject);
console.log(x);

const myString = "Hey";

for (const letter of myString) {
  console.log(letter);
}

const myArray = [true, 10, "abc", null];

for (const element of myArray) {
  console.log(element);
}
