// //объявленое
// function myFn(a, b) {
//   let c;
//   a = a + 1;
//   c = a + b;
//   return c;
// }

// //функциональное
// const myFunction = function (a, b) {
//   let c;
//   a = a + 1;
//   c = a + b;
//   return c;
// };

// console.log(myFn(1, 1));

// //функциональное выражение в вызове другой функции (setTimeout)

// setTimeout(function () {
//   console.log("Отложенное сообщение");
// }, 2000);

// function full() {
//   console.log("i got it personal");
// }

// setTimeout(full, 2000);

// //Arrow function - стрелочная функция

// const arrow = (p1) => {
//   console.log(`arrow function ${p1}`);
// };

// arrow("man");

// setTimeout(() => {
//   console.log("shit");
// }, 2000);

// const short = (a = 4, b = 3) => a + b;

// console.log(short());

function multByFactor(value, muliplier = 1) {
  return value * muliplier;
}

console.log(multByFactor(10, 2));
multByFactor(10, 2);
multByFactor(5);

const newPost = (post, addedAt = Date()) => ({
  ...post,
  addedAt,
});

const firstPost = {
  id: 1,
  author: "Almaz",
};

console.table(newPost(firstPost));

const fnWithError = () => {
  throw new Error("Some error");
};

try {
  fnWithError();
} catch (error) {
  console.log(error);
  console.log(error.message);
}

console.log("Continue");
