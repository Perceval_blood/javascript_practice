// destruct object
const userProfile = {
  name: "Almaz",
  commentsQty: 23,
  hasSingnedAgreement: false,
};

const { name, commentsQty } = userProfile;
const { hasSingnedAgreement } = userProfile;

console.log(name + " " + hasSingnedAgreement);

// destruct array
const userArray = [1, 2, 3];

const [one, two, three] = userArray;
console.log(one);

// destruct buy function
const userInfo = ({ name, commentsQty }) => {
  if (!commentsQty) {
    return `user ${name} has no comments`;
  }
  return `user ${name} has ${commentsQty} comment`;
};

console.log(userInfo(userProfile));
