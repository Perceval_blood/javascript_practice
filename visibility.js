// let a;
// let b;

// function myFn() {
//   let b;
//   a = true;
//   b = 10;
//   console.log(b);
// }

// myFn();

// console.log(a);
// console.log(b);

const a = 5;

function myFn() {
  let a = 4;

  function innerFn() {
    console.log(a);
  }

  innerFn();
}

myFn();
console.log(a);
