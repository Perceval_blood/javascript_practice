const button = {
  width: 200,
  text: "Buy",
  color: "green",
};

const redButton = {
  ...button,
  color: "red",
};

console.log(redButton);

const notChangeButton = {
  color: "red",
  ...button,
};

console.log(notChangeButton);

const buttonInfo = {
  text: "Buy",
};

const buttonStyle = {
  color: "yellow",
  width: 200,
  height: 300,
};

const madeButton = {
  ...buttonInfo,
  ...buttonStyle,
};

console.log(madeButton);
