// const myArr = [1, 3, 3];
// console.log(myArr);

// const myArr2 = new Array(1, 2, 3);
// console.log(myArr2);

// const myArr3 = [...myArr];
// console.log(myArr);

// const myArr4 = [1, true, "a"];
// console.log(myArr4[1]);
// console.log(myArr4.length);

// console.log(myArr4);

//pop, push, shift, unshift, forEach, map
// функции высшего порядка

// -------- push
const myArr5 = [1, 2, 3];
console.log(myArr5);

myArr5.push(4);
console.log(myArr5);

myArr5.push(true);
console.log(myArr5);

// -------- pop
myArr5.pop();
console.log(myArr5);

const removedElement = myArr5.pop();
console.log(removedElement);
console.log(myArr5);

// -------- unshift
myArr5.unshift(false);
console.log(myArr5);

myArr5.unshift({ baby: "boom" });
console.log(myArr5);

// -------- shift
myArr5.shift();
console.log(myArr5);

const removedElementStart = myArr5.shift();
console.log(removedElementStart);
console.log(myArr5);

// -------- forEach

myArr5.forEach((el) => console.log(el * 2));
console.log(myArr5);

// -------- map
// const newArray = myArr5.map(function (el) {
//     return el * 3;
//   });

const newArray = myArr5.map((el) => el * 3);
console.log(newArray);
console.log(myArr5);
